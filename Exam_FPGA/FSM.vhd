-- Quartus II VHDL Template
-- Safe State Machine

library ieee;
use ieee.std_logic_1164.all;

entity FSM is

	port(
		clk	: in std_logic;
		reset : in std_logic;
		load : in std_logic;
		Ack : in std_logic;
		
		Phase1 : out std_logic_vector(1 downto 0);
		Phase2 : out std_logic_vector(1 downto 0);
		Ena_Reg : out std_logic;
		ready : out std_logic;
	);

end entity;

architecture rtl of FSM is

	-- Build an enumerated type for the state machine
	type state_type is (s0, s1);

	-- Register to hold the current state
	signal state   : state_type;

	-- Attribute "safe" implements a safe state machine.
	-- This is a state machine that can recover from an
	-- illegal state (by returning to the reset state).
	attribute syn_encoding : string;
	attribute syn_encoding of state_type : type is "safe";

begin

	-- Logic to advance to the next state
	process (clk, reset)
	begin
		if reset = '0' then
			state <= s0;
		elsif (clk'event and clk='1') then
			case state is
				when s0=>
					if load = '1' then
						state <= s1;
					else
						state <= s0;
					end if;
				when s1=>
					if ready = '1' then
						state <= s2;
					else
						state <= s1;
					end if;
			end case;
		end if;
	end process;

	-- Logic to determine output
	process (state)
	begin
		case state is
			when s0 =>
				output <= "00";
			when s1 =>
				output <= "01";
		end case;
	end process;

end rtl;
