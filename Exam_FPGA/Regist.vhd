library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Regist is
generic(D_W	: integer := 23);
port
(
	-- Input ports
	Data_In : in std_logic_vector((D_W-1) downto 0);
	Enable : std_logic;

	-- Output ports
	Data_Out : out std_logic_vector((D_W-1) downto 0)
);
end Regist;

architecture lat of Regist is
begin
process(Enable, Data_In)
begin
	if (Enable = '1') then
		Data_Out <= Data_In;
	end if;
end process;
end lat;
