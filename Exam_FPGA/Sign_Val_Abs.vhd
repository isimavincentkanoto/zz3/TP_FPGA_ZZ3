library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Sign_Val_Abs is
generic(D_W	: integer := 16);
port
(
	-- Input ports
	clock, reset, Enable : in std_logic;
	Data_In : in std_logic_vector((D_W-1) downto 0);

	-- Output ports
	sign	: out std_logic;
	Val_Abs	: out std_logic_vector((D_W-2) downto 0)
);
end Sign_Val_Abs;

-- Library Clause(s) (optional)
-- Use Clause(s) (optional)

architecture Calcul of Sign_Val_Abs is

	-- Declarations (optional)

begin
process(clock, reset)
	begin
		if(reset = '0') then
			sign <= '0';
			Val_Abs <= (others => '0');
		elsif (clock'event and clock='1') then
			if(Enable = '1') then
				sign <= Data_In(D_W-1);
				Val_Abs <= Data_In((D_W-2) downto 0);
			end if;
		end if;
	end process;
end Calcul;

