library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Int2Float is
generic(D_W	: integer := 16);
port
(
	-- Input ports
	clock, reset, load : in std_logic;
	Int_in : in std_logic_vector((D_W-1) downto 0);

	-- Output ports
	Float_out : out unsigned(31 downto 0);
	ready : out std_logic
);
end Int2Float;

architecture calcul of Int2Float is
begin
	
end calcul;