library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Mantissa_Exp is
generic(D_W	: integer := 15);
port
(
	-- Input ports
	Data_In : in unsigned((D_W-1) downto 0);
	Nb_Zero : in unsigned(3 downto 0);

	-- Output ports
	Mantissa: out unsigned(22 downto 0);
	Exponent : out unsigned(7 downto 0)
);
end Mantissa_Exp;

architecture calcul of Mantissa_Exp is
begin
process(Data_In, Nb_Zero)
	variable size_in_v : integer;
	variable size_end_v : integer;
	begin
	for i in 0 to 13 loop
		if(Nb_Zero=i) then
			size_in_v := D_W - (i+2);
			size_end_v := 22 - size_in_v;
			Mantissa(22 downto size_end_v) <= Data_In(size_in_v downto 0);
			Mantissa((size_end_v - 1) downto 0) <= (others => '0');
			Exponent <= "0" &(6 downto 0 => '1') + i;
		end if;
	end loop;
end process;
end calcul;
