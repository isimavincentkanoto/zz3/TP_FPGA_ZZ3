library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Search_MSB is
generic(D_W	: integer := 15);
port
(
	-- Input ports
	clock, reset, Enable : in std_logic;
	Data_In : in std_logic_vector((D_W-1) downto 0);

	-- Output ports
	ack	: out std_logic;
	Nb_Shift : out unsigned(3 downto 0)
);
end Search_MSB;

-- Library Clause(s) (optional)
-- Use Clause(s) (optional)

architecture Search of Search_MSB is


begin
process(clock, reset)
	variable nb_s_v : unsigned(3 downto 0);
	variable data_v : std_logic_vector((D_W-1) downto 0);
	variable ack_v : std_logic;
	begin
		if(reset='0') then
			nb_s_v := (others => '0');
			data_v := Data_In;
			ack_v := '0';
		elsif(clock'event and clock='1') then
			if(Enable='1' and ack_v='0') then
				if(data_v(D_W-1) = '0') then
					nb_s_v := nb_s_v  + 1;
					data_v := data_v((D_W-2) downto 0) & "0";
				else
					ack_v := '1';
					ack <= ack_v;
				end if;
				Nb_Shift <= nb_s_v;
			end if;
		end if;
	end process;
end Search;
