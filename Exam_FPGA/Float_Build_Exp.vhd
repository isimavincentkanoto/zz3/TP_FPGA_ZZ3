library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Float_Build_Exp is
generic(D_W	: integer := 23);
port
(
	-- Input ports
	Mantissa : in std_logic_vector((D_W-1) downto 0);
	Exponent : in std_logic_vector(7 downto 0);
	Sign : in std_logic;

	-- Output ports
	Float : out std_logic_vector(31 downto 0)
);
end Float_Build_Exp;

architecture calcul of Float_Build_Exp is
begin
	Float(31) <= Sign;
	Float(30 downto 23) <= Exponent;
	Float(22 downto 0) <= Mantissa;
end calcul;
